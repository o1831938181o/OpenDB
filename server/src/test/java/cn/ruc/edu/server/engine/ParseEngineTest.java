package cn.ruc.edu.server.engine;

import cn.ruc.edu.server.api.SQLConstant;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParseEngineTest {

    private final ParseEngine parseEngine = new ParseEngine();

    /**
     * test for create the database
     */
    @Test
    public void test01() {
        boolean flag = parseEngine.execute(SQLConstant.CREATE_DB).isSuccess();
        assertTrue(flag);
    }

    /**
     * test for drop the database
     */
    @Test
    public void test02() {
        boolean flag = parseEngine.execute(SQLConstant.DROP_DB).isSuccess();
        assertTrue(flag);
    }

    /**
     * test for use database
     */
    @Test
    public void test03() {
        boolean flag = parseEngine.execute(SQLConstant.USE_DB).isSuccess();
        assertTrue(flag);
    }

    /**
     * test for create the table
     */
    @Test
    public void test04() {
        test03(); // use it
        boolean flag = parseEngine.execute(SQLConstant.CREATE_TABLE).isSuccess();
        assertTrue(flag);
    }

    /**
     * test for drop table
     */
    @Test
    public void test05() {
        test03(); // use it
        boolean flag = parseEngine.execute(SQLConstant.DROP_TABLE).isSuccess();
        assertTrue(flag);
    }

    /**
     * test for alter table
     */
    @Test
    public void test06() {
        test03(); // use it
        boolean flag = parseEngine.execute(SQLConstant.ALTER_TABLE).isSuccess();
        assertTrue(flag);
    }

    /**
     * test for insert data
     */
    @Test
    public void test07() {
        test03(); // use it
        boolean flag = parseEngine.execute(SQLConstant.INSERT).isSuccess();
        assertTrue(flag);
    }

    /**
     * test for update data
     */
    @Test
    public void test08() {
        test03(); // use it
        boolean flag = parseEngine.execute(SQLConstant.UPDATE).isSuccess();
        assertTrue(flag);
    }

    /**
     * test for delete data
     */
    @Test
    public void test09() {
        test03(); // use it
        boolean flag = parseEngine.execute(SQLConstant.DELETE).isSuccess();
        assertTrue(flag);
    }


    @Test
    public void test10() {
        test03(); // use it;
        var resp = parseEngine.execute(SQLConstant.SELECT);
        String expected = """
                SQLResponse{success=true, data=[
                  {
                    "values": [
                      1.0,
                      "peter",
                      "a",
                      "Jan 10, 2023, 9:24:36 PM"
                    ],
                    "table_name": "stuinfo",
                    "key": 1
                  }
                ]}
                """;
        assertEquals(expected.trim(), resp.toString());
    }
}