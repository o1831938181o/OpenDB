package cn.ruc.edu.server.engine;


import cn.ruc.edu.server.*;
import cn.ruc.edu.server.api.SQLConstant;
import cn.ruc.edu.server.api.SQLResponse;
import cn.ruc.edu.server.entity.DataBase;
import cn.ruc.edu.server.entity.Table;
import cn.ruc.edu.server.utils.JsonUtils;
import com.google.gson.Gson;

import java.util.*;

public class ParseEngine {

    private final DDL ddl = Factory.getDDL();

    private final DML dml = Factory.getDML();

    private final DQL dql = Factory.getDQL();

    private final Gson gson = JsonUtils.getJson();

    public SQLResponse execute(String sql) {
        switch (sql) {
            case SQLConstant.USE_DB -> {
                return new SQLResponse(ddl.useDB("monkey"), null);
            }
            case SQLConstant.CREATE_DB -> {
                DataBase dataBase = new DataBase("monkey", new Date(), null);
                return new SQLResponse(ddl.createDB(dataBase), null);
            }
            case SQLConstant.DROP_DB -> {
                DataBase dataBase = new DataBase("monkey", null, null);
                return new SQLResponse(ddl.dropDB(dataBase), null);
            }
            case SQLConstant.CREATE_TABLE -> {
                Map<Integer, Table.Column> columnMap = new HashMap<>();
                columnMap.put(0, new Table.Column("stuId", Integer.class, false));
                columnMap.put(1, new Table.Column("stuName", String.class, false));
                columnMap.put(2, new Table.Column("gender", Character.class, false));
                columnMap.put(3, new Table.Column("bornDate", Date.class, false));
                Table table = new Table("stuinfo", new Date(), columnMap, Config.db_name);
                return new SQLResponse(ddl.createTable(table), null);
            }
            case SQLConstant.DROP_TABLE -> {
                return new SQLResponse(ddl.dropTable(new Table("stuinfo", null, null, Config.db_name)), null);
            }
            case SQLConstant.ALTER_TABLE -> {
                Map<Integer, Table.Column> columnMap = new HashMap<>();
                columnMap.put(4, new Table.Column("stuId", Integer.class, false));
                Table table = new Table("stuinfo", new Date(), columnMap, Config.db_name);
                return new SQLResponse(ddl.alterTable(table), null);
            }
            case SQLConstant.INSERT -> {
                List<Object> values = new ArrayList<>();
                values.add(1);
                values.add("peter");
                values.add('a'); // a means 男 b means 女
                values.add(new Date());
                Table.Row row = new Table.Row(values, "stuinfo", 1);
                return new SQLResponse(dml.insert(row), null);
            }
            case SQLConstant.UPDATE -> {
                List<Object> values = new ArrayList<>();
                values.add(1);
                values.add("chan");
                values.add('b'); // a means 男 b means 女
                values.add(new Date());
                Table.Row row = new Table.Row(values, "stuinfo", 1);
                return new SQLResponse(dml.update(row), null);
            }
            case SQLConstant.DELETE -> {
                Table.Row row = new Table.Row(null, "stuinfo", 1);
                return new SQLResponse(dml.delete(row), null);
            }
            case SQLConstant.SELECT -> {
                Table table = new Table("stuinfo", null, null, Config.db_name);
                List<Table.Row> rows = dql.select(table);
                return new SQLResponse(true, gson.toJson(rows));
            }
            default -> {
                throw new IllegalArgumentException("do not support");
            }
        }
    }
}
