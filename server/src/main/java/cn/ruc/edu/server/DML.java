package cn.ruc.edu.server;

import cn.ruc.edu.server.entity.Table;

public interface DML {

    /** insert into the spec value to the table **/
    boolean insert(Table.Row row);

    /** update the spec value in the given row **/
    boolean update(Table.Row row);

    /** delete one spec row **/
    boolean delete(Table.Row row);
}