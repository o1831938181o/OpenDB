package cn.ruc.edu.server;

import cn.ruc.edu.server.entity.DataBase;
import cn.ruc.edu.server.entity.Table;

/**
 * Data Definition Language
 */
public interface DDL extends USE {

    /** create the db **/
    boolean createDB(DataBase db);

    /** drop the db **/
    boolean dropDB(DataBase db);

    /** create the table **/
    boolean createTable(Table db);

    /** drop the table **/
    boolean dropTable(Table db);

    /** alter the table **/
    boolean alterTable(Table db);
}
