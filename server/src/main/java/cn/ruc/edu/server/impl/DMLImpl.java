package cn.ruc.edu.server.impl;

import cn.ruc.edu.server.Config;
import cn.ruc.edu.server.DML;
import cn.ruc.edu.server.entity.DataBase;
import cn.ruc.edu.server.entity.Table;
import cn.ruc.edu.server.utils.JsonUtils;
import cn.ruc.edu.server.utils.ResourceUtils;
import com.google.gson.Gson;

import java.util.*;

public final class DMLImpl implements DML {

    private final Gson gson = JsonUtils.getJson();

    @Override
    public boolean insert(Table.Row row) {
        DataBase dataBase = Config.dataBase;
        Set<Table> tables = dataBase.getTables();
        if (tables == null) return false;
        Table table = null;
        for (Table t : tables) {
            if (row.getTable_name().equals(t.getTable_name())) {
                table = t;
                break;
            }
        }
        if (table == null) return false;
        List<Table.Row> rows = table.getRows();
        if (rows == null) {
            rows = new ArrayList<>();
            table.setRows(rows);
        }
        if (checkKey(table, row) && checkType(table, row) && checkEmpty(table, row)) {
            rows.add(row);
        } else {
            return false;
        }
        ResourceUtils.writeContent(table.getDb_name(), gson.toJson(dataBase));
        return true;
    }

    private boolean checkType(Table table, Table.Row row) {
        Map<Integer, Table.Column> columns = table.getColumn();
        List<?> values = row.getValues();
        int len = values.size();
        for (int i = 0; i < len; i++) {
            var value = values.get(i);
            if (value == null) continue;
            if (!columns.get(i).getType().isAssignableFrom(value.getClass())) {
                return false;
            }
        }
        return true;
    }

    private boolean checkKey(Table table, Table.Row row) {
        List<Table.Row> rows = table.getRows();
        for (Table.Row r : rows) {
            if (r.getKey() == row.getKey()) return false;
        }
        return true;
    }

    private boolean checkEmpty(Table table, Table.Row row) {
        Map<Integer, Table.Column> columns = table.getColumn();
        List<?> values = row.getValues();
        int len = values.size();
        for (int i = 0; i < len; i++) {
            var value = values.get(i);
            if (value == null) {
                if (!columns.get(i).isCanEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean update(Table.Row row) {
        DataBase dataBase = Config.dataBase;
        Set<Table> tables = dataBase.getTables();
        if (tables == null) return false;
        Table table = null;
        for (Table t : tables) {
            if (row.getTable_name().equals(t.getTable_name())) {
                table = t;
                break;
            }
        }
        if (table == null) return false;
        List<Table.Row> rows = table.getRows();
        if (rows == null) return false;
        for (Table.Row r : rows) {
            if (r.getKey() == row.getKey()) {
                List<?> values = r.getValues();
                List<Object> valuesCopy = new ArrayList<>(values);
                List<?> changedValues = row.getValues();
                int len = changedValues.size();
                for (int i = 0; i < len; i++) {
                    if (changedValues.get(i) != null) {
                        Object obj = changedValues.get(i);
                        valuesCopy.set(i, obj);
                    }
                }
                r.setValues(valuesCopy);
                if (!checkKey(table, row) && checkType(table, row) && checkEmpty(table, row)) {
                    ResourceUtils.writeContent(table.getDb_name(), gson.toJson(dataBase));
                    return true;
                } else {
                    r.setValues(values); // rollback
                    return false;
                }
            }
        }
        return false;
    }

    @Override
    public boolean delete(Table.Row row) {
        DataBase dataBase = Config.dataBase;
        Set<Table> tables = dataBase.getTables();
        if (tables == null) return false;
        Table table = null;
        for (Table t : tables) {
            if (row.getTable_name().equals(t.getTable_name())) {
                table = t;
                break;
            }
        }
        if (table == null) return false;
        List<Table.Row> rows = table.getRows();
        if (rows == null) return false;
        Iterator<Table.Row> iterator = rows.iterator();
        while (iterator.hasNext()) {
            Table.Row r = iterator.next();
            if (r.getKey() == row.getKey()) {
                iterator.remove();
                ResourceUtils.writeContent(table.getDb_name(), gson.toJson(dataBase));
                return true;
            }
        }
        return false;
    }
}
