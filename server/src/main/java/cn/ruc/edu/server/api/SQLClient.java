package cn.ruc.edu.server.api;

import cn.ruc.edu.server.impl.api.SQLClientImpl;

public interface SQLClient {

    boolean connect();

    SQLResponse execute(String sql);

    /**
     * build class
     **/
    class Builder {

        private final SQLClientImpl sqlClient;

        public Builder() {
            sqlClient = new SQLClientImpl();
        }

        public Builder setUrl(String url) {
            sqlClient.setUrl(url);
            return this;
        }

        public Builder setPort(int port) {
            sqlClient.setPort(port);
            return this;
        }

        public SQLClient build() {
            return sqlClient;
        }
    }
}
