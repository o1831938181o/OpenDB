package cn.ruc.edu.server.exception;

public class IllegalTableException extends IllegalArgumentException {

    public IllegalTableException() {
        super();
    }

    public IllegalTableException(String s) {
        super(s);
    }

    public IllegalTableException(String message, Throwable cause) {
        super(message, cause);
    }
}
