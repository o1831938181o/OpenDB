package cn.ruc.edu.server.exception;

public class IllegalDataBaseException extends IllegalArgumentException {

    public IllegalDataBaseException() {
        super();
    }

    public IllegalDataBaseException(String s) {
        super(s);
    }

    public IllegalDataBaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
