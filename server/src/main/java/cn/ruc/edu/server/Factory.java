package cn.ruc.edu.server;

import cn.ruc.edu.server.impl.DDLImpl;
import cn.ruc.edu.server.impl.DMLImpl;
import cn.ruc.edu.server.impl.DQLImpl;

public final class Factory {
    private static DDL ddl;
    private static DML dml;

    private static DQL dql;

    private Factory() {
    }

    public static synchronized DDL getDDL() {
        if (ddl == null) ddl = new DDLImpl();
        return ddl;
    }

    public static synchronized DML getDML() {
        if (dml == null) dml = new DMLImpl();
        return dml;
    }

    public static synchronized DQL getDQL() {
        if (dql == null) dql = new DQLImpl();
        return dql;
    }
}
