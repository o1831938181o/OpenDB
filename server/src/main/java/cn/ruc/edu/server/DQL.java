package cn.ruc.edu.server;

import cn.ruc.edu.server.entity.Table;

import java.util.List;

public interface DQL {
    List<Table.Row> select(Table table);
}
