package cn.ruc.edu.server.impl;

import cn.ruc.edu.server.Config;
import cn.ruc.edu.server.DQL;
import cn.ruc.edu.server.entity.DataBase;
import cn.ruc.edu.server.entity.Table;

import java.util.List;
import java.util.Set;

public final class DQLImpl implements DQL {

    /** without other condition , only select * **/
    @Override
    public List<Table.Row> select(Table table) {
        DataBase dataBase = Config.dataBase;
        Set<Table> tables = dataBase.getTables();
        if (tables == null) return null;
        if (tables.contains(table)) {
            for (Table t : tables) {
                if (t.equals(table)) {
                    return t.getRows();
                }
            }
        }
        return null;
    }
}
