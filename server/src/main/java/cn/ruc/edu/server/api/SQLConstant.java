package cn.ruc.edu.server.api;

/**
 * constant for execute
 * <p>
 * because we expect that start from zero to one to build a string engine is really difficult,
 * we assume every command is known by users
 */
public interface SQLConstant {
    /**
     * ddl impl
     */

    String USE_DB = "use database monkey";
    String CREATE_DB = "create database monkey";
    String DROP_DB = "drop monkey";
    String CREATE_TABLE = """
                CREATE TABLE stuinfo(
                    stuId INT,#学生编号
                    stuName VARCHAR(20),#姓名
                    gender CHAR,#性别
                    bornDate DATETIME#生日
                );
            """;
    String DROP_TABLE = "drop table stuinfo";

    /**
     * now only support add a column
     */
    String ALTER_TABLE = """
            alter table stuinfo add hobby VARCHAR(20)
                        
            """;

    /**
     * dml impl
     */
    String INSERT = """
            insert into stuinfo values (1,"peter","男","2000-1-1")
            """;

    String UPDATE = """
            update stuinfo set name = "chan" gender = "女" where id = '1'
            """;

    String DELETE = """
            delete stuinfo where id = '1'
            """;

    String SELECT = """
            select * from stuinfo
            """;
}
