package cn.ruc.edu.server;

import cn.ruc.edu.opendb.rpc.OpenDBServiceGrpc;
import cn.ruc.edu.opendb.rpc.Reply;
import cn.ruc.edu.opendb.rpc.Request;
import cn.ruc.edu.server.api.SQLResponse;
import cn.ruc.edu.server.engine.ParseEngine;
import cn.ruc.edu.server.utils.JsonUtils;
import com.google.gson.Gson;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Server {

    private static final Logger logger = Logger.getLogger(Server.class.getName());

    private io.grpc.Server server;

    private void start() throws IOException {
        /* The port on which the server should run */
        int port = 50051;
        server = ServerBuilder.forPort(port)
                .addService(new OpenDBServiceImpl())
                .build()
                .start();
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                try {
                    Server.this.stop();
                } catch (InterruptedException e) {
                    e.printStackTrace(System.err);
                }
                System.err.println("*** server shut down");
            }
        });
    }

    private void stop() throws InterruptedException {
        if (server != null) {
            server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        final Server server = new Server();
        server.start();
        server.blockUntilShutdown();
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    static class OpenDBServiceImpl extends OpenDBServiceGrpc.OpenDBServiceImplBase {

        ParseEngine mParseEngine = new ParseEngine();

        private final Gson gson = JsonUtils.getJson();

        @Override
        public void exec(Request request, StreamObserver<Reply> responseObserver) {
            String command = request.getCommand();
            SQLResponse response = mParseEngine.execute(command);
            String msg = gson.toJson(response);
            System.out.println(msg);
            System.out.println("================msg send===================");
            Reply reply = Reply.newBuilder().setMessage(msg).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }
    }
}
