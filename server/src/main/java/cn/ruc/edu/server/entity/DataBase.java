package cn.ruc.edu.server.entity;


import java.util.Date;
import java.util.Set;

public final class DataBase {

    private String db_name;

    private Date lastModify;

    private Set<Table> tables;

    public DataBase(String db_name, Date lastModify, Set<Table> tables) {
        this.db_name = db_name;
        this.lastModify = lastModify;
        this.tables = tables;
    }

    public String getName() {
        return db_name;
    }

    public void setName(String db_name) {
        this.db_name = db_name;
    }

    public Date getLastModify() {
        return lastModify;
    }

    public void setLastModify(Date lastModify) {
        this.lastModify = lastModify;
    }

    public Set<Table> getTables() {
        return tables;
    }

    public void setTables(Set<Table> tables) {
        this.tables = tables;
    }
}
