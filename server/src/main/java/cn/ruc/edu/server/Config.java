package cn.ruc.edu.server;

import cn.ruc.edu.server.entity.DataBase;

/**
 * config hold for database
 */
public final class Config {
    public static String db_name;
    public static DataBase dataBase;
}
