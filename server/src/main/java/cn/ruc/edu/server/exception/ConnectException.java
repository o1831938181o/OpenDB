package cn.ruc.edu.server.exception;

public class ConnectException extends IllegalArgumentException {

    public ConnectException() {
        super();
    }

    public ConnectException(String s) {
        super(s);
    }

    public ConnectException(String message, Throwable cause) {
        super(message, cause);
    }
}
