package cn.ruc.edu.server.entity;

import com.google.common.base.Objects;

import java.util.Date;
import java.util.List;
import java.util.Map;

public final class Table {

    private String table_name;

    private Date lastModify;

    private Map<Integer, Column> column;

    private List<Row> rows;

    private String db_name;

    public Table(String table_name, Date lastModify, Map<Integer, Column> column, String db_name) {
        this.table_name = table_name;
        this.lastModify = lastModify;
        this.column = column;
        this.db_name = db_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Table table = (Table) o;
        return Objects.equal(table_name, table.table_name) && Objects.equal(db_name, table.db_name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(table_name, db_name);
    }

    public String getDb_name() {
        return db_name;
    }

    public void setDb_name(String db_name) {
        this.db_name = db_name;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public Date getLastModify() {
        return lastModify;
    }

    public void setLastModify(Date lastModify) {
        this.lastModify = lastModify;
    }

    public Map<Integer, Column> getColumn() {
        return column;
    }

    public void setColumn(Map<Integer, Column> column) {
        this.column = column;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public static class Column {
        private String column_name;

        private Class<?> type;

        private boolean canEmpty;

        public Column(String column_name, Class<?> type, boolean canEmpty) {
            this.column_name = column_name;
            this.type = type;
            this.canEmpty = canEmpty;
        }

        public String getColumn_name() {
            return column_name;
        }

        public void setColumn_name(String column_name) {
            this.column_name = column_name;
        }

        public Class<?> getType() {
            return type;
        }

        public void setType(Class<?> type) {
            this.type = type;
        }

        public boolean isCanEmpty() {
            return canEmpty;
        }

        public void setCanEmpty(boolean canEmpty) {
            this.canEmpty = canEmpty;
        }
    }


    public static class Row {
        private List<?> values;

        private String table_name;

        private int key;

        public Row(List<?> values, String table_name, int key) {
            this.values = values;
            this.table_name = table_name;
            this.key = key;
        }

        public int getKey() {
            return key;
        }

        public void setKey(int key) {
            this.key = key;
        }

        public List<?> getValues() {
            return values;
        }

        public void setValues(List<?> values) {
            this.values = values;
        }

        public String getTable_name() {
            return table_name;
        }

        public void setTable_name(String table_name) {
            this.table_name = table_name;
        }
    }
}
