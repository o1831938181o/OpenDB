package cn.ruc.edu.server.impl.api;

import cn.ruc.edu.server.api.SQLClient;
import cn.ruc.edu.server.api.SQLResponse;
import cn.ruc.edu.server.engine.ParseEngine;
import cn.ruc.edu.server.exception.ConnectException;

public class SQLClientImpl implements SQLClient {

    private final ParseEngine mParseEngine;

    public SQLClientImpl() {
        this.mParseEngine = new ParseEngine();
    }

    private String url;

    private int port;

    private boolean connect = false;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPort(int port) {
        this.port = port;
    }


    @Override
    public boolean connect() {
        return connect;
    }

    @Override
    public SQLResponse execute(String sql) {
        if (!connect) throw new ConnectException("error connect");
        mParseEngine.execute(sql);
        return null;
    }
}
