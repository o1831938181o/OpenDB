package cn.ruc.edu.server.impl;

import cn.ruc.edu.server.Config;
import cn.ruc.edu.server.DDL;
import cn.ruc.edu.server.entity.DataBase;
import cn.ruc.edu.server.entity.Table;
import cn.ruc.edu.server.utils.JsonUtils;
import cn.ruc.edu.server.utils.ResourceUtils;
import com.google.gson.Gson;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public final class DDLImpl implements DDL {

    private final Gson gson = JsonUtils.getJson();

    @Override
    public boolean useDB(String db_name) {
        DataBase db = gson.fromJson(ResourceUtils.getContent(db_name), DataBase.class);
        if (db == null) return false;
        Config.dataBase = db;
        Config.db_name = db_name;
        return true;
    }

    @Override
    public boolean createDB(DataBase db) {
        String content = gson.toJson(db);
        return ResourceUtils.writeContent(db.getName(), content);
    }

    @Override
    public boolean dropDB(DataBase db) {
        return ResourceUtils.deleteContent(db.getName());
    }

    @Override
    public boolean createTable(Table table) {
        DataBase db = Config.dataBase;
        Set<Table> tables = db.getTables();
        if (tables == null) {
            tables = new HashSet<>();
            db.setTables(tables);
        }
        if (tables.contains(table)) return false;
        table.setLastModify(new Date());
        tables.add(table);
        ResourceUtils.writeContent(table.getDb_name(), gson.toJson(db));
        return true;
    }

    @Override
    public boolean dropTable(Table table) {
        DataBase db = Config.dataBase;
        Set<Table> tables = db.getTables();
        System.out.println(tables);
        if (tables == null) return false;
        boolean flag = tables.remove(table);
        if (flag) ResourceUtils.writeContent(table.getDb_name(), gson.toJson(db));
        return flag;
    }

    @Override
    public boolean alterTable(Table table) {
        DataBase db = Config.dataBase;
        Set<Table> tables = db.getTables();
        if (tables == null) return false;
        if (!tables.contains(table)) return false;
        Table original = null;
        for (Table tt : tables) {
            if (tt.hashCode() == table.hashCode()) {
                original = tt;
                break;
            }
        }
        boolean flag = computeDiff(original, table);
        if (flag) ResourceUtils.writeContent(table.getDb_name(), gson.toJson(db));
        return flag;
    }

    private boolean computeDiff(Table original, Table modified) {
        if (modified.getTable_name() != null) {
            original.setTable_name(modified.getTable_name());
        }
        modified.getColumn().forEach((key, value) -> {
            original.getColumn().put(key, value);
        });
        original.setLastModify(new Date());
        return true;
    }
}
