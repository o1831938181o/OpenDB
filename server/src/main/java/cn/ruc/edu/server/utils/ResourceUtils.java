package cn.ruc.edu.server.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public final class ResourceUtils {

    private ResourceUtils() { }

    private final static String dir = "db" + File.separator;

    private final static String suffix = ".db";


    public static String getContent(String db_name) {
        File file = new File(dir + db_name + suffix);
        try {
            return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean writeContent(String db_name, String content) {
        File file = new File(dir + db_name + suffix);
        try {
            FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean deleteContent(String db_name) {
        File file = new File(dir + db_name + suffix);
        try {
            FileUtils.delete(file);
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
