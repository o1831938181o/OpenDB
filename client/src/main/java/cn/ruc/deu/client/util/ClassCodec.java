package cn.ruc.deu.client.util;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * serializer for class
 */
public class ClassCodec implements JsonSerializer<Class<?>>, JsonDeserializer<Class<?>> {
    public static void main(String[] args) {
        Gson gson = new GsonBuilder().registerTypeAdapter(Class.class, new ClassCodec()).create();
        System.out.println(gson.toJson(int.class));
    }

    @Override
    public Class<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            String str = json.getAsString();
            switch (str) {
                case "byte" -> {
                    return byte.class;
                }
                case "short" -> {
                    return short.class;
                }
                case "int" -> {
                    return int.class;
                }
                case "long" -> {
                    return long.class;
                }
                case "float" -> {
                    return float.class;
                }
                case "double" -> {
                    return double.class;
                }
                case "boolean" -> {
                    return boolean.class;
                }
                case "char" -> {
                    return char.class;
                }
            }
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new JsonParseException(e);
        }
    }

    @Override
    public JsonElement serialize(Class<?> src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getName());
    }
}
