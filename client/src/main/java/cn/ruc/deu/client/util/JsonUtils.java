package cn.ruc.deu.client.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class JsonUtils {
    private JsonUtils() {
    }

    private static Gson gson;

    public static synchronized Gson getJson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .registerTypeAdapter(Class.class, new ClassCodec())
                    .create();
        }
        return gson;
    }

}
