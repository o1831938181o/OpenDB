package cn.ruc.deu.client.api;

import com.google.common.base.MoreObjects;

public final class SQLResponse {
    private final boolean success;

    private String data;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("success", success)
                .add("data", data)
                .toString();
    }

    public SQLResponse(boolean success, String data) {
        this.success = success;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getData() {
        return data;
    }
}
