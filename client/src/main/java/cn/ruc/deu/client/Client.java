package cn.ruc.deu.client;

import cn.ruc.deu.client.api.SQLConstant;
import cn.ruc.deu.client.api.SQLResponse;
import cn.ruc.deu.client.util.JsonUtils;
import cn.ruc.edu.opendb.rpc.OpenDBServiceGrpc;
import cn.ruc.edu.opendb.rpc.Reply;
import cn.ruc.edu.opendb.rpc.Request;
import com.google.gson.Gson;
import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    private static final Logger logger = Logger.getLogger(Client.class.getName());

    private static final Gson gson = JsonUtils.getJson();
    private final OpenDBServiceGrpc.OpenDBServiceBlockingStub blockingStub;

    public Client(Channel channel) {
        blockingStub = OpenDBServiceGrpc.newBlockingStub(channel);
    }

    public String exec(String sql) {
        Request request = Request.newBuilder().setCommand(sql).build();
        Reply response;
        try {
            response = blockingStub.exec(request);
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            return null;
        }
        return response.getMessage();
    }

    public static void main(String[] args) throws Exception {
        String use = SQLConstant.USE_DB;
        String sql = SQLConstant.SELECT;
        String target = "localhost:50051";

        ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
                .usePlaintext()
                .build();
        try {
            Client client = new Client(channel);

            String ans = null;

            ans = client.exec(SQLConstant.DROP_DB);
            System.out.println(ans);
            ans = client.exec(SQLConstant.CREATE_DB);
            System.out.println(ans);
            ans = client.exec(SQLConstant.USE_DB);
            System.out.println(ans);

            ans = client.exec(SQLConstant.CREATE_TABLE);
            System.out.println(ans);
            ans = client.exec(SQLConstant.ALTER_TABLE);
            System.out.println(ans);
            ans = client.exec(SQLConstant.DROP_TABLE);
            System.out.println(ans);
            ans = client.exec(SQLConstant.CREATE_TABLE);
            System.out.println(ans);

            ans = client.exec(SQLConstant.INSERT);
            System.out.println(ans);
            ans = client.exec(SQLConstant.UPDATE);
            System.out.println(ans);
            ans = client.exec(SQLConstant.SELECT);
            System.out.println(ans);
            ans = client.exec(SQLConstant.DELETE);
            System.out.println(ans);

        } finally {
            channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}
